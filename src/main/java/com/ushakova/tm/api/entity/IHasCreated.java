package com.ushakova.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getDateCreate();

    void setDateCreate(Date date);

}
