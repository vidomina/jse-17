package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    void remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}