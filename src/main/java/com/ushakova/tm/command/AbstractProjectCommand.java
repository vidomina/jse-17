package com.ushakova.tm.command;

import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProjectInfo(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId()
                + "\nTitle: " + project.getName()
                + "\nDescription: " + project.getDescription()
                + "\nStatus: " + project.getStatus().getDisplayName()
                + "\nStart Date: " + project.getDateStart()
                + "\nExpiration Date: " + project.getDateFinish()
                + "\nCreate Date: " + project.getDateCreate());
    }

}
