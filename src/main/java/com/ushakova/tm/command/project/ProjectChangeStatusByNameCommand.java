package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change project status by name.";
    }

    public void execute() {
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService().changeStatusByName(name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "change-project-status-by-name";
    }

}
