package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectCompleteProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"Complete\" status to project by index.";
    }

    public void execute() {
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().completeProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "complete-project-by-index";
    }

}
