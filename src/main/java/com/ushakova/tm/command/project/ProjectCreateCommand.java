package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    public void execute() {
        System.out.println("***Project Create***");
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-create";
    }

}
