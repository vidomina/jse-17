package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectRemoveOneByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    public void execute() {
        System.out.println("***Remove Project***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().removeOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

}
