package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectStartProjectByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"In Progress\" status to project by name.";
    }

    public void execute() {
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "start-project-by-name";
    }

}
