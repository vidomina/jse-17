package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("\n***About***");
        System.out.println("Name: Valentina Ushakova");
        System.out.println("E-MailL: vushakova@tsconsulting.com");
    }

    @Override
    public String name() {
        return "about";
    }

}
