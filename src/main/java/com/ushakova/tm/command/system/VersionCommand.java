package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("\n***Version***");
        System.out.println("1.0.0");
    }

    @Override
    public String name() {
        return "version";
    }

}
