package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;

public class TaskRemoveOneByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    public void execute() {
        System.out.println("***Remove Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "task-remove-by-id";
    }

}
