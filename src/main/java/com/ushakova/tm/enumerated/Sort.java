package com.ushakova.tm.enumerated;

import com.ushakova.tm.comparator.ComparatorByCreated;
import com.ushakova.tm.comparator.ComparatorByDateStart;
import com.ushakova.tm.comparator.ComparatorByName;
import com.ushakova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort By Created", ComparatorByCreated.getInstance()),
    DATE_START("Sort By Start", ComparatorByDateStart.getInstance()),
    NAME("Sort By Name", ComparatorByName.getInstance()),
    STATUS("Sort By Status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }
}
