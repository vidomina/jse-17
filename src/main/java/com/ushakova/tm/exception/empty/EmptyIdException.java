package com.ushakova.tm.exception.empty;

import com.ushakova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("An error has occurred: id is empty.");
    }

}
