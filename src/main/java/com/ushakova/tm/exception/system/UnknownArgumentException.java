package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(String argument) {
        super("***Error: Argument " + argument + " Not Found***");
    }

}
